# frozen_string_literal: true

require 'spec_helper'

describe 'select issues by iteration' do
  include_context 'with integration context'

  let!(:issue_without_iteration) do
    issue.merge(description: 'issue without iteration')
  end
  let!(:issue_with_iteration) do
    issue.merge(description: 'issue with any iteration', iid: issue[:iid] + 1)
  end
  let!(:issue_with_iteration_10) do
    issue.merge(description: 'issue with iteration 10', iid: issue[:iid] + 2)
  end

  describe 'Without iteration' do
    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues",
        query: { per_page: 100, iteration_id: 'None' },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [issue_without_iteration]
      end
    end

    it 'comments on the issue' do
      rule = <<~YAML
        resource_rules:
          issues:
            rules:
              - name: Test rule
                conditions:
                  iteration: None
                actions:
                  comment: |
                    Comment because it does not belong to an iteration.
      YAML

      stub_post_issue_without_iteration = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue_without_iteration[:iid]}/notes",
        body: { body: 'Comment because it does not belong to an iteration.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_issue_without_iteration)
    end
  end

  describe 'With any iteration' do
    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues",
        query: { per_page: 100, iteration_id: 'Any' },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [issue_with_iteration]
      end
    end

    it 'comments on the issue' do
      rule = <<~YAML
        resource_rules:
          issues:
            rules:
              - name: Test rule
                conditions:
                  iteration: Any
                actions:
                  comment: |
                    Comment because it does belong to an iteration.
      YAML

      stub_post_issue_with_iteration = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue_with_iteration[:iid]}/notes",
        body: { body: 'Comment because it does belong to an iteration.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_issue_with_iteration)
    end
  end

  describe 'With a given iteration' do
    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues",
        query: { per_page: 100, iteration_title: 'Iteration 10' },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [issue_with_iteration_10]
      end
    end

    it 'comments on the issue' do
      rule = <<~YAML
        resource_rules:
          issues:
            rules:
              - name: Test rule
                conditions:
                  iteration: Iteration 10
                actions:
                  comment: |
                    Comment because it does belong to iteration 10.
      YAML

      stub_post_issue_with_iteration_10 = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue_with_iteration_10[:iid]}/notes",
        body: { body: 'Comment because it does belong to iteration 10.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_issue_with_iteration_10)
    end
  end
end
